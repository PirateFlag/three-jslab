import Vue from "vue";
import VueRouter from "vue-router";
import ViewPage from "@/components/viewpage/viewpage";
import Welcome from "@/components/welcome/welcome";
import BaseCloth from "@/views/cloth/cloth";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Welcome",
    component: Welcome,
  },
  {
    path: "/base/",
    name: "base",
    component: ViewPage,
    children: [
      {
        path: "cloth",
        name: "cloth",
        component: BaseCloth,
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
