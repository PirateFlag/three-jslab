const getters = {
  serve_url: (state) => state.app.server_url,
  menuList: (state) => state.menu.menuList,
};

export default getters;
