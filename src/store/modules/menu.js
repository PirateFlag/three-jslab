const menu = {
  state: {
    menuList: [
      {
        title: "基础渲染",
        index: "1",
        icons: "el-icon-message",
        link: "",
        children: [
          {
            title: "cloth(服装渲染)",
            index: "1-1",
            link: "/base/cloth",
            icons: "",
            children: [],
          },
          {
            title: "keyframe(动画场景)",
            index: "1-2",
            link: "/base/frame",
            icons: "",
            children: [],
          },
          {
            title: "blending(动画任务)",
            index: "1-3",
            link: "",
            icons: "",
            children: [],
          },
        ],
      },
      {
        title: "相机",
        index: "2",
        icons: "el-icon-message",
        link: "",
        children: [
          {
            title: "相机视角",
            index: "2-1",
            link: "",
            icons: "",
            children: [],
          },
        ],
      },
    ],
  },
  mutations: {},
  action: {},
};

export default menu;
