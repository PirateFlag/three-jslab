import { SERVER_URL } from "@/store/mutation-types.js";
const app = {
  state: {
    server_url: SERVER_URL,
  },
  mutations: {},
  action: {},
};

export default app;
