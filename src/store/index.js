import Vue from "vue";
import Vuex from "vuex";

import app from "./modules/app"; // 主框架全局数据
import menu from "./modules/menu"; // 菜单数据
import getters from "./getters";
Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    app,
    menu,
  },
  getters,
  state: {},
  mutations: {},
  actions: {},
});
